#lang racket

(require "mk.rkt")

(define reversibleo
  (lambda (r1 r2 c1 c2)
    (conde
     [(== r1 c1) (== r2 c2)]
     [(== r1 c2) (== r2 c1)])))

(define membero
  (lambda (x l)
    (fresh (x^ l^)
      (== l `(,x^ . ,l^))
      (conde
       [(== x x^)]
       [(membero x l^)]))))


(define oneway_transitions
  '())

(define both_transitions
  '((DC . EC) (EC . PO)
    (PO . TPP)     (PO . TPPi) (PO . EQ)
    (TPP . NTPP)   (TPP . EQ)  (NTPP . EQ)
    (TPPi . NTPPi) (TPPi . EQ) (NTPPi . EQ)))

(define (unique l)
  (define (remove x l)
    (if (null? l)
        l
        (let ((x^ (car l))
              (l^ (cdr l)))
          (if (equal? x x^)
              (remove x l^)
              (cons x^ (remove x l^))))))
  (if
    (null? l)
    l
    (let ((x (car l))
          (l^ (cdr l)))
      (cons x (unique (remove x l^))))))

(define transitions
  (unique (append oneway_transitions
                  both_transitions
                  (map (lambda (x) (cons (cdr x) (car x))) both_transitions))))

(define transitiono
  (lambda (r1 r2)
    (fresh (x)
      (== x `(,r1 . ,r2))
      (membero x transitions))))


(define moveo
  (lambda (r1 r2 path)
    (conde
     [(== r1 r2) (== path `(,r1))]
     [(fresh (r1^ path^)
        (transitiono r1 r1^)
        (== path `(,r1 . ,path^))
        (moveo r1^ r2 path^))])))


(run* (A B C D E) (moveo 'EQ E `(,A ,B ,C ,D ,E)))

(run 6 (P) (moveo 'EC 'NTPPi P))



